import sys
from UserClasses.Users import  Users
from UserClasses.Admin import Admin
from UserClasses.Client import Client
from UserClasses.Seller import Seller

class CreateUsers():
    "Class CreateUsers i opoia perilamvanei ti main()"
    '''
    def __main__(self, username, name, surname, role):
        client = Client("johnjohn6", "john", "kalo", "client", "012")
        seller = Seller("tzaneio", "nikos", "tzan", "seller")
        admin = Admin("shields", "giwrgos", "aspio", "admin")
        user1 = Users(username, name, surname, role)
        #askisi 01.2
        user2 = Users(raw_input("Enter user username:"), raw_input("Enter user name:"),
                      raw_input("Enter user surname:"), raw_input("Enter user role:"))

        #calling the methods to test if the objects are initialized correctly

        # client.login()
        # client.logout()
        # client.payment()
        # client.showAccount()
        # client.showCallHistory()
        #
        # seller.addClient()
        # seller.changeClientProgram()
        # seller.login()
        # seller.logout()
        # seller.printBill()
        #
        # admin.createClient(client)
        # admin.createSeller(seller)
        # admin.login()
        # admin.logout()
        # admin.removeClient(client)
        # admin.removeSeller(seller)
        #
        # user1.login()
        # user1.logout()
        #
        # user2.login()
        # user2.logout()


        #calling the initializeClient method to test if it is working correctly

        #self.initializeClient()



        #calling the readAndSaveClients method to test if it workong properly
        #self.readAndSaveClients("clientInfo.csv")

    '''
    #askisi 01.3.1 - elegxoume an to afm einai int. Ta upoloipa pedia tha
    #einai etsi ki alliws strings
    def initializeClient(self):
        try:
            client2 = Client(raw_input("Enter client username:"), raw_input("Enter client name:"),
                        raw_input("Enter client surname:"), raw_input("Enter client role:"),
                        int(raw_input("Enter client afm:")))
        except ValueError as e:
            print "The following exception occured: ", e.message
            print "Try again: \n"
            self.initializeClient()
        else:
            client2.login()
            client2.logout()
            client2.payment()
            client2.showAccount()
            client2.showCallHistory()
        finally:
            print "client initialized from keyboard input..."

    #askisi 4.2 - 4.2.1
    def readAndSaveClients(self, fileName):
        try:
            #open the file by reading the filename given and reading the contents
            fin = open (fileName, "r")
            data = fin.readlines()
            i = 0
            for line in data:
                #splitting the lines
                fields = line.split(",")
                fileClient = Client(fields[0], fields[1], fields[2], fields[3], fields[4])
                #throws ValueError if the afm is not an integer
                int(fields[4])
                #creating the new files' names dynamically
                i += 1
                fileNameFields = fileName.split(".")
                newFilename = fileNameFields[0] + str(i) + "." + fileNameFields[1]
                #opening the new file and wrinting the client values
                fout = open (newFilename, "w+")
                fout.write("Username of the client:  " + fields[0] + "\n" +
                           "Name of the client:     " + fields[1] + "\n" +
                           "Surname of the client:  " + fields[2] + "\n" +
                           "Role of the client:     " + fields[3] + "\n" +
                           "afm of the client:      " + fields[4] + "\n")
                fout.close()
        except IOError as e:
            #clopei paste apo to documentation tis python gia na mporesoume na
            #doume tis plirofories sxetika me to IOException
             print "I/O error({0}): {1}".format(e.errno, e.strerror)
        except NameError as e:
            #provlimata me metavlites
            print "Name error: {0}".format(e.message)
        except ValueError as e:
            #an to afm dn einai arithmos
            print "afm is not an integer... \n"
            print "ValueError: {0}".format(e.message)
        except:
            #epistrefei plirofories sxetika me to exception pou etuxe
            print "Unexpected Error: ", sys.exc_info()[0]
        finally:
            fin.close()

'''
if __name__ == "__main__":
    cu = CreateUsers()
    #having imported the sys module we get the arguments given by the user
    #with the sys.argv list
    cu.__main__(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
'''
