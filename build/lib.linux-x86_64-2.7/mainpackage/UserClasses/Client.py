from sqlalchemy.orm import Query
from sqlalchemy import text
from mainpackage.dao import User, Bill, Telephony_Program, Call
from datetime import datetime
from mainpackage.Helpers import Helper

class Client(object):
    '''
    Class Client which contains methods for the corresponding functions that the client will perform.
    The functions are the following 1. show account info, 2. show call history, 3. execute account payment
    '''
    def __init__(self, _session):
                self.session = _session

    # old getAccountInfo, which uses text statements
    # def getAccountInfo(self, client_id):
    #         """Shows the account information by performing query on the database."""
    #         try:
    #             # xrisimopoioume textual sql gt den uparxei kindunos sql injection kai gia exaskisi
    #
    #             #get account info for specific user_id
    #             stmt1 = text (  """
    #                             select bill.teleph_number, users.firstname, users.lastname, telephony_program.charge_mob, telephony_program.charge_home, telephony_program.charge_standard, telephony_program.program_desc
    #                             from users
    #                             inner join bill
    #                             on users.user_id = bill.user_id
    #                             inner join telephony_program
    #                             on bill.program_id = telephony_program.program_id
    #                             where users.user_id =:id;
    #                             """)
    #
    #             stmt1 = stmt1.columns(Bill.teleph_number, User.firstname, User.lastname,
    #                                 Telephony_Program.charge_mob, Telephony_Program.charge_home,
    #                                 Telephony_Program.charge_standard, Telephony_Program.program_desc)
    #
    #             result_info = self.session.query(Bill.teleph_number, User.firstname, User.lastname,
    #                                 Telephony_Program.charge_mob, Telephony_Program.charge_home,
    #                                 Telephony_Program.charge_standard,
    #                                 Telephony_Program.program_desc).from_statement(stmt1).params(id=client_id).all()
    #
    #             # get all calls for specific user_id and specific date
    #             stmt2 = text(
    #                 """
    #                 select call.call_dur, call.called_number, call.call_date
    #                 from users inner join bill
    #                 on users.user_id = bill.user_id
    #                 inner join call
    #                 on call.bill_id = bill.bill_id
    #                 where users.user_id =:id and extract (month from call.call_date) =:month;
    #                 """
    #             )
    #
    #             stmt2 = stmt2.columns(Call.call_dur, Call.called_number, Call.call_date)
    #
    #             #pairnoume ton logariasmo tou proigoumenou mina mono
    #             result_calls = self.session.query(Call.call_dur, Call.called_number,
    #                                         Call.call_date).from_statement(stmt2).params(id = client_id, month = datetime.now().month - 1).all()
    #
    #             #get overall cost for last month
    #             h = Helper()
    #             overall_cost = h.calculateOverallAccountCost(calls = result_calls, mobile_cost = result_info[0].charge_mob,
    #                                           home_cost = result_info[0].charge_home, standard_cost =result_info[0].charge_standard)
    #
    #         except Exception as e:
    #             raise
    #
    #         # teliko apotelesma pou periexei arithmo tilefwnou, onoma, epwnumo, perigrafi programatos,
    #         # sunoliko kostos
    #         result_final = [result_info[0], result_info[1], result_info[2], result_info[6, overall_cost]]
    #
    #         return result_final


    def getAccountInfo2(self, client_id):
        """Shows the account information by performing queries on the database."""
        try:
            #get account info for specific user_id using sqlalchemy query API
            result_info = self.session.query( User.firstname, User.lastname, Bill.teleph_number,
                                    Telephony_Program.charge_mob, Telephony_Program.charge_home,
                                    Telephony_Program.charge_standard,
                                    Telephony_Program.program_desc).join(Bill, User.user_id == Bill.user_id).join(Telephony_Program, Bill.program_id == Telephony_Program.program_id).filter(User.user_id == client_id).all()

            # get all calls for specific user_id and specific date
            result_calls = self.session.query(User.user_id, Bill.bill_id, Call.call_dur, Call.called_number ,
                                    Call.call_date).join(Bill, User.user_id == Bill.user_id).join(Call, Bill.bill_id == Call.bill_id).filter(User.user_id == client_id).all()
                                    #, Call.call_date.month == datetime.now().month - 1

            result_call_final = []
            for result_call in result_calls:
                if result_call.call_date.month == datetime.now().month - 1:
                    result_call_final.append(result_call)

            h = Helper()
            # to sunoliko kostos gia olous tous logariasmous me ena logariasmo ana xristi
            # TODO: na pairnoume ti sunoliki xrewsi me vasi kathe logariasmo
            overallcost = h.calculateOverallAccountCost (calls = result_call_final, mobile_cost = result_info[0].charge_mob,
                                                                home_cost = result_info[0].charge_home, standard_cost =result_info[0].charge_standard)

            # teliko apotelesma pou periexei arithmo tilefwnou, onoma, epwnumo, perigrafi programatos,
            # sunoliko kostos
            result_final = [result_info[0].teleph_number, result_info[0].firstname, result_info[0].lastname, result_info[0].program_desc, overallcost]

        except Exception as e:
            raise

        return result_final


    def showCallHistory(self, bill_id_filter, date_filter, called_number_filter):
        #shows called_number. call_duration and date of call.
        #--> maybe add a column of charge per call?
        #--> add filtering for calls
        #--> get client bill_id on helper method
        try:
            if (date_filter == "" and called_number_filter == ""):
                q = self.session.query(Call).filter(Call.bill_id == bill_id_filter).all()
            elif (date_filter == ""):
                q = self.session.query(Call).filter(Call.bill_id == bill_id_filter,
                                               Call.called_number == called_number_filter).all()
            elif (called_number_filter == ""):
                q = self.session.query(Call).filter(Call.bill_id == bill_id_filter,
                                               Call.call_date == date_filter).all()
            else:
                q = self.session.query(Call).filter(Call.bill_id == bill_id_filter,
                                               Call.call_date == date_filter,
                                               Call.called_number == called_number_filter).all()
        except Exception as e:
            raise
        #q = session.query(Call).all()
        print p

    def payment(self, user_id_arg2, payment_amt):
        h = Helper()
        bill_id = h.getLastBillId(_session = self.session, user_id_arg = user_id_arg2)

        try:
            #pairnoume to overall due tou pelati kai kanoume tin afairesi bill_overall_due - payment_amt
            overall_cost_list = self.session.query(Bill.bill_overall_due).filter(Bill.bill_id == bill_id).order_by(Bill.bill_id.desc()).first()
            overall_cost = overall_cost_list[0]

            final_overall_due = overall_cost - payment_amt

            q = self.session.query(Bill).filter(Bill.bill_id == bill_id).order_by(Bill.bill_id.desc()).first()
            q.bill_overall_due = final_overall_due
            self.session.commit()
        except Exception as e:
            raise
